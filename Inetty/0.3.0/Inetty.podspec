Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '8.0'
s.name = "Inetty"
s.summary = "Networking library"
s.requires_arc = true

# 2
s.version = "0.3.0"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "Raffaele D'Alterio'" => "thecutter@bornluckystudio.com" }

# 5 - Replace this URL with your own Github page's URL (from the address bar)
s.homepage = "http://bornluckystudio.com"

# For example,
# s.homepage = "https://github.com/JRG-Developer/RWPickFlavor"

# For example,
s.source = { :git => "https://RaffaeleD@bitbucket.org/bornluckystudio/inetty.git", :tag => "#{s.version}"}


# 7
s.frameworks = "UIKit", "CoreData"

# 8
s.source_files = "Inetty/**/*.{swift}"

end
